from django.contrib import admin
from django.core.urlresolvers import reverse

from .models import *


def url_to_edit_object(item, link=None):
    url = reverse('admin:%s_%s_change' % (item._meta.app_label,  item._meta.model_name),  args=[item.id] )
    return '<a href="%s">%s</a>' % (url,  link or item)


class ProjectPackageAdmin(admin.ModelAdmin):
    list_display = ('package_name', 'project_name', )
    list_filter = ('package_name', 'project_name', )
    readonly_fields = ('package_name', 'project_name',)


class SourceFileAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'library_name', )
    list_filter = ('type', 'library_name',)
    search_fields = ('name',)
    readonly_fields = ('relative_path', 'include_targets', 'included_by')

    def include_targets(self, obj):
        str = '<div>'
        for item in obj.as_origin.all().order_by('target__library_name', 'target__type', 'target__name',):
            str += '%s<br/>' % (url_to_edit_object(item.target, item.target.get_relpath()))
        str += '</div>'
        return str
    include_targets.allow_tags = True

    def included_by(self, obj):
        str = '<div>'
        for item in obj.as_target.all().order_by('origin__library_name', 'origin__type', 'origin__name',):
            str += '%s<br/>' % (url_to_edit_object(item.origin, item.origin.get_relpath()))
        str += '</div>'
        return str
    included_by.allow_tags = True

    def relative_path(self, obj):
        return obj.get_relpath()
    

class LibraryAdmin(admin.ModelAdmin):
    list_display = ('name', 'package', 'is_test_library')
    list_filter = ('package', 'is_test_library',)
    readonly_fields = ('id', 'sourcefiles',)

    def sourcefiles(self, obj):
        str = '<div>'
        for item in obj.sourcefile_set.all().order_by('type', 'name',):
            str += '%s<br/>' % (url_to_edit_object(item, item.name))
        str += '</div>'
        return str
    sourcefiles.allow_tags = True


class IncludeRelationshipAdmin(admin.ModelAdmin):
    list_display = ('origin', 'target', 'type',)
    list_filter = ('type',)


# Register your models here.
admin.site.register(Project)
admin.site.register(Package)
admin.site.register(ProjectPackage, ProjectPackageAdmin)
admin.site.register(Library, LibraryAdmin)
admin.site.register(SourceFile, SourceFileAdmin)
admin.site.register(IncludeRelationship, IncludeRelationshipAdmin)

