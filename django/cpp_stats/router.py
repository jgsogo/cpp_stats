# DB router for app1

APP_LABEL = 'cpp_stats'
DB_NAME = 'cpp_stats'

class DBRouter(object):
    """
    A router to control APP_LABEL db operations
    """
    def db_for_read(self, model, **hints):
        "Point all operations on cpp_stats models to DB_NAME"
        if model._meta.app_label == APP_LABEL:
            return DB_NAME
        return None

    def db_for_write(self, model, **hints):
        "Point all operations on app1 models to 'DB_NAME"
        if model._meta.app_label == APP_LABEL:
            return DB_NAME
        return None

    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a model in APP_LABEL is involved"
        if obj1._meta.app_label == APP_LABEL or obj2._meta.app_label == APP_LABEL:
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        "Make sure the APP_LABEL app only appears on the DB_NAME db"
        if app_label == APP_LABEL:
            return db == DB_NAME
        return None
