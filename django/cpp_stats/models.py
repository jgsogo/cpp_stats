from __future__ import unicode_literals

import os
from django.db import models


class Project(models.Model):
    name = models.CharField(primary_key=True, max_length=256)
    path = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'project'

    def __str__(self):
        return self.name


class Package(models.Model):
    name = models.CharField(primary_key=True, max_length=256)
    path = models.CharField(max_length=1024, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'package'

    def __str__(self):
        return self.name


class ProjectPackage(models.Model):
    id = models.IntegerField(primary_key=True)
    project_name = models.ForeignKey(Project, db_column='project__name')  # Field renamed because it contained more than one '_' in a row.
    package_name = models.ForeignKey(Package, db_column='package__name')  # Field renamed because it contained more than one '_' in a row.

    class Meta:
        managed = False
        db_table = 'project_package'


class Library(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=256)
    package = models.ForeignKey(Package, db_column='package')
    path = models.CharField(max_length=1024, blank=True, null=True)
    is_test_library = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'library'

    def __str__(self):
        return self.name


class SourceFile(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=256, blank=True, null=True)
    library_name = models.ForeignKey(Library, db_column='library__name')  # Field renamed because it contained more than one '_' in a row.
    path = models.CharField(max_length=1024, blank=True, null=True)
    type = models.CharField(max_length=256, blank=True, null=True)

    includes = models.ManyToManyField('self', through='IncludeRelationship', symmetrical=False)

    def __str__(self):
        return self.name

    def get_relpath(self):
        return "[{}] {}".format(self.library_name, os.path.join(self.path, self.name))

    class Meta:
        managed = False
        db_table = 'source_file'


class IncludeRelationship(models.Model):
    id = models.IntegerField(primary_key=True)
    origin = models.ForeignKey(SourceFile, db_column='origin', related_name='as_origin')
    target = models.ForeignKey(SourceFile, db_column='target', related_name='as_target')
    type = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'include_relationship'






