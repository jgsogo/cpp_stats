from django.apps import AppConfig


class CppStatsConfig(AppConfig):
    name = 'cpp_stats'
