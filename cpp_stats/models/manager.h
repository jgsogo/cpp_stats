
#pragma once

#include "queryset/backends/sqlite3.h"
#include "config_store.h"

namespace cpp_stats {

	template <typename TModel>
	class SQLite3Manager : public qs::sqlite3::Manager<TModel> {
		public:
		    SQLite3Manager() : qs::sqlite3::Manager<TModel>() {}
	};
}

namespace qs {
	namespace sqlite3 {

		template <typename TModel>
		sqlite::connection& Manager<TModel>::get_connection() {
			return cpp_stats::ConfigStore::get().get_connection();
		}

	} 
}
