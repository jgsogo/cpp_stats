
#include "include_relationship.h"

namespace qs {
	namespace sqlite3 {
		template<> const std::string Manager <cpp_stats::IncludeRelationship>::_table_name = "include_relationship";
		template<> const std::array<std::string, 4> Manager<cpp_stats::IncludeRelationship>::_column_names = {"id", "origin", "target", "type"};
	}
}

namespace cpp_stats {

	std::string IncludeRelationshipTypes::to_string() const {
		switch (_choices) {
		case IncludeRelationshipTypes::INTRA_LIB: return "intra_lib";
		case IncludeRelationshipTypes::INTRA_PCK: return "intra_pck";
		case IncludeRelationshipTypes::OUTER_PCK: return "outer_pck";
		case IncludeRelationshipTypes::UNKNOWN: return "unknown";
		default:
			std::ostringstream os;
			os << "None(" << _choices << ")";
			return os.str();
		}
	}

	std::istream& operator >> (std::istream& is, IncludeRelationshipTypes& w) {
		std::string token; is >> token;
		if (token == "intra_lib") { w._choices = IncludeRelationshipTypes::INTRA_LIB; }
		else if (token == "intra_pck") { w._choices = IncludeRelationshipTypes::INTRA_PCK; }
		else if (token == "outer_pck") { w._choices = IncludeRelationshipTypes::OUTER_PCK; }
		else if (token == "unknown") { w._choices = IncludeRelationshipTypes::UNKNOWN; }
		else {
			spdlog::get("cpp_stats")->error("Invalid IncludeRelationshipTypes string '{}'.", token);
			w._choices = IncludeRelationshipTypes::UNKNOWN;
		}
		return is;
	}

	std::ostream& operator << (std::ostream& os, const IncludeRelationshipTypes& w) {
		os << w.to_string();
		return os;
	}

	IncludeRelationship::IncludeRelationship() {}
	IncludeRelationship::IncludeRelationship(const BaseModel::tuple& data) : IncludeRelationship::BaseModel(data) {}
	IncludeRelationship::~IncludeRelationship() {}

	IncludeRelationshipManager<IncludeRelationship>::IncludeRelationshipManager() : SQLite3Manager<IncludeRelationship>() {}

	void IncludeRelationshipManager<IncludeRelationship>::add(const SourceFile& origin, const SourceFile& target) {
		auto qs = this->all().filter(std::make_tuple(OriginSourceFile(origin), TargetSourceFile(target)));
		
		// guess type
		IncludeRelationshipTypes type = IncludeRelationshipTypes::OUTER_PCK;
		auto origin_lib = origin.get<Library>();
		auto target_lib = target.get<Library>();
		if (origin_lib == target_lib) {
			type = IncludeRelationshipTypes::INTRA_LIB;
		}
		else if (origin_lib.get<Package>() == target_lib.get<Package>()) {
			type = IncludeRelationshipTypes::INTRA_PCK;
		}

		if (qs.empty()) {
			static std::size_t pk = 0;
			IncludeRelationship include_relation(std::make_tuple(pk++, OriginSourceFile(origin), TargetSourceFile(target), type));
			this->insert(include_relation);
		}
		else {
			// TODO: need more features in queryset-cpp
			/*
			auto& item = qs[0];
			auto value = item.get<2>();
			value += 1;
			*/
		}
	}

}

