
#pragma once

#include <iostream>
#include <boost/filesystem.hpp>

#include "queryset/models/model.h"
#include "queryset/strong_typedef.h"
#include "manager.h"


namespace cpp_stats {

	typedef ::utils::strong_typedef<std::string, 0> package_name;

	template <class TModel>
	class PackageManager;

	class Package : public qs::BaseModel<Package, PackageManager, package_name, boost::filesystem::path> {
		public:
			Package();
			Package(const BaseModel::tuple& data);
			virtual ~Package();

			std::string get_name() const;
			boost::filesystem::path get_path() const;
	};

	template <>
	class PackageManager<Package> : public SQLite3Manager<Package> {
		public:
			PackageManager();
			Package parse(const boost::filesystem::path&);
	};

}
