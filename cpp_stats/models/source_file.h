
#pragma once

#include <iostream>
#include <boost/filesystem.hpp>

#include "queryset/models/model.h"
#include "manager.h"
#include "library.h"


namespace cpp_stats {

    template <class TModel>
    class SourceFileManager;

    struct SourceFileTypes {
        enum Types { HEADER, SOURCE, UNKNOWN };
        SourceFileTypes() : _choices(UNKNOWN) {}
        SourceFileTypes(const Types& c) : _choices(c) {}

        std::string to_string() const;
        friend bool operator==(const SourceFileTypes& lhs, const SourceFileTypes& rhs) {
            return lhs._choices == rhs._choices;
        }

        friend bool operator<(const SourceFileTypes& lhs, const SourceFileTypes& rhs) {
            return lhs._choices < rhs._choices;
        }

        Types _choices;
    };

	std::istream& operator >> (std::istream& is, SourceFileTypes& w);
    std::ostream& operator << (std::ostream& os, const SourceFileTypes& w);


    class SourceFile : public qs::BaseModel<SourceFile, SourceFileManager, std::size_t, std::string, Library, boost::filesystem::path, SourceFileTypes> {
        public:
            SourceFile();
            SourceFile(const BaseModel::tuple& data);
            virtual ~SourceFile();

			boost::filesystem::path get_filepath() const;
			std::vector<boost::filesystem::path> parse_includes() const;
    };

	std::ostream& operator << (std::ostream& os, const SourceFile& w);

    template <>
    class SourceFileManager<SourceFile> : public SQLite3Manager<SourceFile> {
        public:
            SourceFileManager();
            bool add_to_library(const Library& lib, const boost::filesystem::path&, bool commit = true);
    };
}
