
#pragma once

#include <iostream>
#include <boost/filesystem.hpp>

#include "package.h"
#include "settings.h"


namespace cpp_stats {

	typedef ::utils::strong_typedef<std::string, 0> library_name;
	//typedef ::utils::strong_typedef<std::string, 1> library_path;

    template <class TModel>
    class LibraryManager;

    class Library : public qs::BaseModel<Library, LibraryManager, std::size_t, library_name, Package, boost::filesystem::path, bool> {
        public:
            Library();
            Library(const BaseModel::tuple& data);
            virtual ~Library();

			std::string get_name(bool include_pck = true) const;
    };

    template <>
    class LibraryManager<Library> : public SQLite3Manager<Library> {
        public:
            LibraryManager();
            Library parse(const Package&, const boost::filesystem::path&, PackageParserConfig&, bool is_test_libary = false);
    };

}
