
#include "settings.h"

#include <boost/filesystem.hpp>
#include "spdlog/common.h"
#include "rapidjson/document.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/error/en.h"

#include "config_store.h"


namespace cpp_stats {

	namespace {
		bool read_package_config(const boost::filesystem::path& pck_path, rapidjson::Document& doc) {
			auto config_file = (pck_path / "cpp_stats.json");
			if (boost::filesystem::exists(config_file)) {
				FILE* pFile = fopen(config_file.string().c_str(), "rb");
				char buffer[65536];
				rapidjson::FileReadStream is(pFile, buffer, sizeof(buffer));
				doc.ParseStream(is);
				if (doc.HasParseError()) {
					std::stringstream os; os << "Parse error '";
					os << rapidjson::GetParseError_En(doc.GetParseError());
					os << "' at position " << doc.GetErrorOffset() << ".";
					throw std::runtime_error(os.str());
				}
				return true;
			}
			return false;
		}
	}

	static const std::string ignore_dirs_str = "ignore_dirs";
	static const std::string unittest_library_pattern_str = "unittest_library_pattern";

	PackageParserConfig::PackageParserConfig(const boost::filesystem::path& package_path) {
		// Read from root settings (may be overriden by package specific ones)
		if (ConfigStore::get().has_value(ignore_dirs_str)) {
			_ignore_dirs = ConfigStore::get().get_values(ignore_dirs_str);
		}
		if (ConfigStore::get().has_value(unittest_library_pattern_str)) {
			_unittest_pattern = ConfigStore::get().get_value(unittest_library_pattern_str);
		}

		// Read config from certain filename inside package (before reading specific info from settings)
		rapidjson::Document pck_config;
		if (read_package_config(package_path, pck_config)) {
			if (pck_config.HasMember(ignore_dirs_str.c_str())) {
				throw std::logic_error("Not implemented! PackageParserConfig::PackageParserConfig");
			}
			if (pck_config.HasMember(unittest_library_pattern_str.c_str())) {
				throw std::logic_error("Not implemented! PackageParserConfig::PackageParserConfig");
			}
		}
		else {
			auto pck_name = package_path.filename();
			
			auto specific_pck_ignore_dirs_token = "packages/" + pck_name.string() + "/" + ignore_dirs_str;
			if (ConfigStore::get().has_value(specific_pck_ignore_dirs_token)) {
				auto add_ignores = ConfigStore::get().get_values(specific_pck_ignore_dirs_token);
				std::copy(add_ignores.begin(), add_ignores.end(), std::back_inserter(_ignore_dirs));
			}

			auto specific_pck_unittest_pattern = "packages/" + pck_name.string() + "/" + unittest_library_pattern_str;
			if (ConfigStore::get().has_value(specific_pck_unittest_pattern)) {
				_unittest_pattern = ConfigStore::get().get_value(specific_pck_unittest_pattern);
			}
		}
	}

	const std::vector<std::string>& PackageParserConfig::ignore_dirs() {
		return _ignore_dirs;
	}

	const std::string& PackageParserConfig::unittest_library_pattern() {
		return _unittest_pattern;
	}

}