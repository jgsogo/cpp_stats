
#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <sqlite3cc.h>


namespace cpp_stats {

    class ConfigStore {
        public:
            static ConfigStore& get();

            void parse_file(const std::string& filename);
            void parse_data(const std::string& data);

			sqlite::connection& get_connection() const;

            template<typename T>
            T get_value_as(const std::string& key) const {
                std::stringstream ss(this->get_value(key));
                T t; ss >> t;
                return t;
            }

			std::string get_value(const std::string& key) const;
			std::vector<std::string> get_values(const std::string& key) const;
			bool has_value(const std::string& key) const;

        private:
            ConfigStore();
            ~ConfigStore();
            
            struct Data;
            Data* _data;
    };

}
