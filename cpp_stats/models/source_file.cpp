
#include "source_file.h"

#include <sstream>
#include <regex>
#include "spdlog/spdlog.h"
#include "config_store.h"

namespace qs {
	namespace sqlite3 {
		template<> const std::string Manager<cpp_stats::SourceFile>::_table_name = "source_file";
		template<> const std::array<std::string, 5> Manager<cpp_stats::SourceFile>::_column_names = { "id", "name", "library__name", "path", "type" };
	}
}


namespace cpp_stats {

    std::string SourceFileTypes::to_string() const {
        switch (_choices) {
            case SourceFileTypes::HEADER: return "header";
            case SourceFileTypes::SOURCE: return "source";
            case SourceFileTypes::UNKNOWN: return "unknown";
            default:
                std::ostringstream os;
                os << "None(" << _choices << ")";
                return os.str();
        }
    }

	std::istream& operator >> (std::istream& is, SourceFileTypes& w) {
		std::string token; is >> token;
		if (token == "header") { w._choices = SourceFileTypes::HEADER; }
		else if (token == "source") { w._choices = SourceFileTypes::SOURCE; }
		else if (token == "unknown") { w._choices = SourceFileTypes::UNKNOWN; }
		else {
			spdlog::get("cpp_stats")->error("Invalid SourceFileTypes string '{}'.", token);
			w._choices = SourceFileTypes::UNKNOWN; 
		}
		return is;
	}

    std::ostream& operator << (std::ostream& os, const SourceFileTypes& w) {
        os << w.to_string();
        return os;
    }


    SourceFile::SourceFile() {}
    SourceFile::SourceFile(const BaseModel::tuple& data) : SourceFile::BaseModel(data) {}
    SourceFile::~SourceFile() {}

	std::ostream& operator << (std::ostream& os, const SourceFile& w)
	{
		w.print(os);
		return os;
	}

	boost::filesystem::path SourceFile::get_filepath() const {
		auto lib = this->get<Library>();
		auto lib_path = lib.get<boost::filesystem::path>();
		auto me = this->get<boost::filesystem::path>() / this->get<std::string>();
		return boost::filesystem::canonical(lib_path / me);
	}

	std::vector<boost::filesystem::path> SourceFile::parse_includes() const {
		std::ifstream input(this->get_filepath().string());
		std::vector<boost::filesystem::path> ret;
		std::regex re_include("^\\s*#\\s*include\\s+((?:<[^>]*>|\"[^\"]*\"))\\s*(?://.*)?");
		std::smatch base_match;
		for (std::string line; std::getline(input, line);)
		{
			if (std::regex_match(line, base_match, re_include)) {
				auto match = base_match[1].str();
				auto included_file = match.substr(1, match.length() - 2);
				ret.push_back(included_file);
			}
		}
		return ret;
	}

    SourceFileManager<SourceFile>::SourceFileManager() : SQLite3Manager<SourceFile>() {}

    bool SourceFileManager<SourceFile>::add_to_library(const Library& lib, const boost::filesystem::path& path, bool commit) {
		auto console = spdlog::get("cpp_stats");

        auto filename = path.filename();
        auto ext = boost::filesystem::extension(path);
        auto file_type = SourceFileTypes::UNKNOWN;
        if (ext == ".h" || ext == ".hpp") {
            file_type = SourceFileTypes::HEADER;
        }
        else if (ext == ".c" || ext == ".cpp") {
            file_type = SourceFileTypes::SOURCE;
        }

        if (file_type != SourceFileTypes::UNKNOWN) {
            const auto& lib_path = lib.get<boost::filesystem::path>();
			auto rel_path = boost::filesystem::relative(path, lib_path).parent_path();
			static std::size_t pk = 0; // TODO: Try something for primary key field (issue to queryset-cpp).
            SourceFile file(std::make_tuple(pk++, filename.string(), lib, (rel_path.empty() ? "." : rel_path), file_type));
			this->insert(file);
			console->debug(" - [{}]\t{}", file.get<boost::filesystem::path>(), file.get<std::string>());
            return true;
        }
        return false;
    }

}