
#pragma once

#include <iostream>
#include <boost/filesystem.hpp>

#include "queryset/models/model.h"
#include "source_file.h"

namespace cpp_stats {

	template <class TModel>
	class IncludeRelationshipManager;

	typedef ::utils::strong_typedef<SourceFile, 0> OriginSourceFile;
	typedef ::utils::strong_typedef<SourceFile, 1> TargetSourceFile;

	struct IncludeRelationshipTypes {
		enum Types { INTRA_LIB, INTRA_PCK, OUTER_PCK, UNKNOWN }; // TODO: add TEST2LIB (but before we need to know which library a test library links to.
		IncludeRelationshipTypes() : _choices(UNKNOWN) {}
		IncludeRelationshipTypes(const Types& c) : _choices(c) {}

		std::string to_string() const;
		friend bool operator==(const IncludeRelationshipTypes& lhs, const IncludeRelationshipTypes& rhs) {
			return lhs._choices == rhs._choices;
		}

		friend bool operator<(const IncludeRelationshipTypes& lhs, const IncludeRelationshipTypes& rhs) {
			return lhs._choices < rhs._choices;
		}

		Types _choices;
	};

	std::istream& operator >> (std::istream& is, IncludeRelationshipTypes& w);
	std::ostream& operator << (std::ostream& os, const IncludeRelationshipTypes& w);

    class IncludeRelationship : public qs::BaseModel<IncludeRelationship, IncludeRelationshipManager, std::size_t, OriginSourceFile, TargetSourceFile, IncludeRelationshipTypes> {
        public:
            IncludeRelationship();
			IncludeRelationship(const BaseModel::tuple& data);
            virtual ~IncludeRelationship();
    };

	template <>
	class IncludeRelationshipManager<IncludeRelationship> : public SQLite3Manager<IncludeRelationship> {
		public:
			IncludeRelationshipManager();
			void add(const SourceFile& origin, const SourceFile& target);
	};
}
