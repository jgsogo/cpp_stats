
#include "package.h"
#include "spdlog/spdlog.h"
#include "config_store.h"

#include "library.h"


namespace qs {
	namespace sqlite3 {
		template<> const std::string Manager<cpp_stats::Package>::_table_name = "package";
		template<> const std::array<std::string, 2> Manager<cpp_stats::Package>::_column_names = { "name", "path" };
	}
}

namespace cpp_stats {

	Package::Package() {}
	Package::Package(const BaseModel::tuple& data) : Package::BaseModel(data) {}
	Package::~Package() {}

	std::string Package::get_name() const {
		return this->get<package_name>().value;
	}
	boost::filesystem::path Package::get_path() const {
		return this->get<boost::filesystem::path>();
	}

	PackageManager<Package>::PackageManager() : SQLite3Manager<Package>() {}

	Package PackageManager<Package>::parse(const boost::filesystem::path& path) {
		auto console = spdlog::get("cpp_stats");

		// 1) Package name 
		auto name = boost::filesystem::basename(path);
		console->info("Parse package '{}' at '{}'", name, path.string());

		Package pck(std::make_tuple(package_name(name), path.string()));
		this->insert(pck);

		PackageParserConfig parser_config(path);
		auto& ignore_dirs = parser_config.ignore_dirs();

        boost::filesystem::directory_iterator end_it;
        for (boost::filesystem::directory_iterator it(path); it != end_it; ++it)
        {
            if (boost::filesystem::is_directory(it->status()))
            {
                auto folder_name = it->path().filename();
                if ((folder_name.string()[0] == '.') ||
					std::find(ignore_dirs.begin(), ignore_dirs.end(), folder_name.string()) != ignore_dirs.end())
                {
                    continue;
                }

                console->debug("Found library: '{}' at '{}'", folder_name.string(), it->path().string());
                Library::objects().parse(pck, it->path(), parser_config);
            }
        }
		return pck;
	}
}
