
#include "library.h"

#include <regex>
#include "spdlog/spdlog.h"
#include "config_store.h"
#include "source_file.h"

namespace qs {
	namespace sqlite3 {
		template<> const std::string Manager<cpp_stats::Library>::_table_name = "library";
		template<> const std::array<std::string, 5> Manager<cpp_stats::Library>::_column_names = { "id", "name", "package", "path", "is_test_library" };
	}
}

namespace cpp_stats {

    Library::Library() {}
    Library::Library(const BaseModel::tuple& data) : Library::BaseModel(data) {}
    Library::~Library() {}

	std::string Library::get_name(bool include_pck) const {
		if (include_pck) {
			return this->get<Package>().get_name() + "/" + this->get<library_name>().value;
		}
		return this->get<library_name>().value;
	}

    LibraryManager<Library>::LibraryManager() : SQLite3Manager<Library>() {}

    Library LibraryManager<Library>::parse(const Package& pck, const boost::filesystem::path& path, PackageParserConfig& parser_config, bool is_test_libary) {
        auto console = spdlog::get("cpp_stats");

        // 1) Library name 
        auto name = path.filename().string();
		console->info("Parse library '{}' at '{}'", name, path.string());

		static std::size_t pk = 0;
		Library lib(std::make_tuple(pk++, library_name(name), pck, path.string(), is_test_libary));
		this->insert(lib);

		auto& ignore_dirs = parser_config.ignore_dirs();
		auto& unittest_pattern = parser_config.unittest_library_pattern();

        // 2) Work on files
        std::size_t files = 0;
        boost::filesystem::recursive_directory_iterator end_it;
        for (boost::filesystem::recursive_directory_iterator it(path); it != end_it; ++it)
        {
            if (boost::filesystem::is_directory(it->status()))
            {
                auto folder_name = it->path().filename();
                if ((folder_name.string()[0] == '.') ||
					std::find(ignore_dirs.begin(), ignore_dirs.end(), folder_name.string()) != ignore_dirs.end())
                {
                    it.no_push();
                    continue;
                }
				if (!unittest_pattern.empty()) {
					auto path_rel = boost::filesystem::relative(it->path(), pck.get_path());
					std::regex unittest_path_re(unittest_pattern);
					if (std::regex_match(path_rel.string(), unittest_path_re)) {
						this->parse(pck, it->path(), parser_config, true);
						it.no_push();
						continue;
					}
				}
            }
            else if (boost::filesystem::is_regular_file(it->status()))
            {
                files += SourceFile::objects().add_to_library(lib, it->path());
            }
        }
        console->debug("\tgot {} files", files);
        return lib;
    }
}
