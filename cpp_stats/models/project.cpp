
#include "project.h"

#include "spdlog/spdlog.h"
#include "config_store.h"
#include "source_file.h"
#include "include_relationship.h"


namespace qs {
	namespace sqlite3 {
		template<> const std::string Manager<cpp_stats::Project>::_table_name = "project";
		template<> const std::array<std::string, 2> Manager<cpp_stats::Project>::_column_names = { "name", "path" };

		template<> const std::string Manager<cpp_stats::ProjectPackage>::_table_name = "project_package";
		template<> const std::array<std::string, 3> Manager<cpp_stats::ProjectPackage>::_column_names = { "id", "project__name", "package__name" };
	}
}

namespace {
	namespace fs = boost::filesystem;
	void include_dirs(std::vector<fs::path>& paths) {
		if (cpp_stats::ConfigStore::get().has_value("include_dirs")) {
			auto dirs = cpp_stats::ConfigStore::get().get_values("include_dirs");
			for (auto& dir : dirs) {
				paths.push_back(dir);
			}
		}
	}

	bool found_file(const fs::path& filename, const fs::path& ref_file, const std::vector<fs::path>& additional_paths, fs::path& output) {
		// Try to find it using 'ref_file':
		if (fs::exists(ref_file.parent_path() / filename)) {
			output = fs::canonical(ref_file.parent_path() / filename);
			return true;
		}
		// Try using the other paths
		for (auto& p : additional_paths) {
			if (fs::exists(p / filename)) {
				output = fs::canonical(p / filename);
				return true;
			}
		}
		return false;
	}

}

namespace cpp_stats {

    Project::Project() {}
    Project::Project(const BaseModel::tuple& data) : Project::BaseModel(data) {}
    Project::~Project() {}

	void Project::populate_deps() {
		auto console = spdlog::get("cpp_stats");

		// Get additional include dirs
		auto packages_path = this->get<boost::filesystem::path>();  // Project path
		if (ConfigStore::get().has_value("packages_path")) {
			packages_path /= ConfigStore::get().get_value_as<boost::filesystem::path>("packages_path");
		}
		std::vector<boost::filesystem::path> base_paths;
		base_paths.push_back(packages_path);
		include_dirs(base_paths);

		// auto libprojs = ProjectLibrary::objects().all().filter<Project>(*this);
		auto sources_by_lib = SourceFile::objects().all().groupBy<Library>();
		for (auto& library_files : sources_by_lib) {
			// TODO: Filter by libraries in this project (may use ProjectLibrary)
			console->debug("\tLibrary {}", library_files.first);
			for (auto& source : library_files.second) {
				console->debug("\t\t {}", source.get<std::string>());
				auto includes = source.parse_includes();

				// For each include I need to find it in filesystem.
				boost::filesystem::path include_path;
				for (auto& include : includes) {
					if (found_file(include, source.get_filepath(), base_paths, include_path)) {
						console->debug("\t\t\t -> {}", include_path);
						// If it exists, then it may belong to one of the libraries
						for (auto& libs : sources_by_lib) {
							auto rel_path = fs::relative(include_path, libs.first.get<boost::filesystem::path>());
							auto it_found = std::find_if(libs.second.begin(), libs.second.end(), [&include_path](auto& source_file) {
								return include_path == source_file.get_filepath();
							});
							if (it_found != libs.second.end()) {
								console->debug("\t\t\t\t ...found on library '{}'", libs.first.get_name());
								IncludeRelationship::objects().add(source, *it_found);
							}
						}
					}
				}
			}
		}

	}

    ProjectManager<Project>::ProjectManager() : SQLite3Manager<Project>() {}

    Project ProjectManager<Project>::parse_project(const boost::filesystem::path& path_)
    {
        auto console = spdlog::get("cpp_stats");
		auto path = boost::filesystem::canonical(path_);

		// 1) Project name 
        auto name = boost::filesystem::basename(path);
        Project project(std::make_tuple(name, path.string()));
		this->insert(project);

        // 2) Look for libraries
		auto libraries_path = path;
		if (ConfigStore::get().has_value("packages_path")) {
			libraries_path /= ConfigStore::get().get_value_as<boost::filesystem::path>("packages_path");
		}

		PackageParserConfig parser_config(path);
		auto& ignore_dirs = parser_config.ignore_dirs();

		console->info("Parse project '{}' at '{}'", name, libraries_path.string());
        boost::filesystem::directory_iterator end_it;
        for (boost::filesystem::directory_iterator it(libraries_path); it != end_it; ++it)
        {
            if (boost::filesystem::is_directory(it->status()))
            {
                auto folder_name = it->path().filename();
                if ((folder_name.string()[0] == '.') ||
					std::find(ignore_dirs.begin(), ignore_dirs.end(), folder_name.string()) != ignore_dirs.end())
                {
                    continue;
                }

                console->debug("Found package: '{}' at '{}'", folder_name.string(), it->path().string());
                auto pck = Package::objects().parse(it->path());
				static std::size_t pk = 0; // TODO: Autoincremental pk?
                auto projpck = ProjectPackage(std::make_tuple(pk++, project, pck));
				ProjectPackage::objects().insert(projpck);
            }
        }
        return project;
    }


	ProjectPackage::ProjectPackage() {}
	ProjectPackage::ProjectPackage(const BaseModel::tuple& data) : ProjectPackage::BaseModel(data) {}
	ProjectPackage::~ProjectPackage() {}
    
}