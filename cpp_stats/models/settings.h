
#pragma once

#include <string>
#include <vector>

#include <boost/filesystem/path.hpp>

namespace cpp_stats {
	
	class PackageParserConfig {
		public:
			PackageParserConfig(const boost::filesystem::path& package_path);

			const std::vector<std::string>& ignore_dirs();
			const std::string& unittest_library_pattern();

		protected:
			std::vector<std::string> _ignore_dirs;
			std::string _unittest_pattern;
	};

}
