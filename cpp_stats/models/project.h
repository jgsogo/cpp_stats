
#pragma once

#include <iostream>
#include <boost/filesystem.hpp>

#include "queryset/models/model.h"
#include "manager.h"
#include "package.h"


namespace cpp_stats {

    template <class TModel>
    class ProjectManager;

    class Project : public qs::BaseModel<Project, ProjectManager, std::string, boost::filesystem::path> {
        public:
            Project();
            Project(const BaseModel::tuple& data);
            virtual ~Project();

			void populate_deps();
    };

    template <>
    class ProjectManager<Project> : public SQLite3Manager<Project> {
        public:
            ProjectManager();
            Project parse_project(const boost::filesystem::path&);
    };


    class ProjectPackage : public qs::BaseModel<ProjectPackage, SQLite3Manager, std::size_t, Project, Package> {
        public:
			ProjectPackage();
			ProjectPackage(const BaseModel::tuple& data);
            virtual ~ProjectPackage();
    };

}
