project(CPPStats)
cmake_minimum_required(VERSION 2.8.12)


add_subdirectory(models)
add_subdirectory(commands)
