
#include <iostream>
#include <boost/program_options.hpp>
#include <boost/algorithm/string/join.hpp>

#include "../log_level_param.hpp"
#include "../../models/project.h"
#include "../../models/config_store.h"

#include "../../models/include_relationship.h"
#include "../../models/library.h"
#include "../../models/project.h"
#include "../../models/source_file.h"

#include "parse_cmake.hpp"
#include "generate_graph.hpp"


using namespace cpp_stats;

int main(int argc, char** argv) {
    std::cout << "== Generate graph ==\n";
    try {
        // Define and parse the program options
        namespace po = boost::program_options;
        po::options_description desc("Options");
		bool override_output(false);
        desc.add_options()
            ("help", "Print help messages")
            ("log-level,l", po::value<log_level>()->default_value(log_level(spdlog::level::info)), std::string("log level (" + log_level::options() + ")").c_str())
            ("settings", po::value<std::string>()->required(), "path to settings file")
            ("project", po::value<std::string>()->required(), "Name of project")
            ("cmake-graph", po::value<boost::filesystem::path>()->required(), "Path to CMake generated project dependencies ('.dot' file)")
            ("output-filename", po::value<boost::filesystem::path>(), "Output path (if not given will use console)")
			("override", po::bool_switch(&override_output)->default_value(false), "override 'output-filename' if it already exists")
            ;

        po::variables_map vm;
        try {
            po::store(po::parse_command_line(argc, argv, desc), vm); // can throw
                                                                     // handle help option
            if (vm.count("help")) {
                std::cout << "Basic Command Line Parameter App" << std::endl
                    << desc << std::endl;
                return 0;
            }
            po::notify(vm); // throws on error, so do after help in case there are any problems             
        }
        catch (po::error& e) {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return -1;
        }

        // Get logger level (initialize also child loggers)
        spdlog::level::level_enum log_level = vm["log-level"].as<::log_level>()._level;
        //#ifdef SPDLOG_DEBUG_ON
        spdlog::stdout_logger_mt("qs")->set_level(log_level);
        //#endif
        auto console = spdlog::stdout_logger_mt("cpp_stats");
        console->set_level(log_level);

        // Get settings
        namespace fs = boost::filesystem;
        fs::path settings = vm["settings"].as<std::string>();
        std::cout << " - path to settings: '" << settings << "'\n";
        console->info("Configure C++ Stats: '{}'", settings.string());
        ConfigStore::get().parse_file(settings.string());

        // Parse CMake file
        auto cmake_graph_filename = vm["cmake-graph"].as<fs::path>();
        console->info("Parse CMake file with library dependencies '{}'", cmake_graph_filename);
		labels_t cmake_labels, cpp_labels;
		auto cmake_graph = read_graphviz(cmake_graph_filename, cmake_labels);
		links_t cpp_graph;
        
        // Parse project
        auto project_name = vm["project"].as<std::string>();
        auto project = Project::objects().all()[0];
        console->info("Generate graph for project '{}'", project);
		auto projpacks = ProjectPackage::objects().all().filter<Project>(project);
		for (auto& pck : projpacks) {
			auto libs = Library::objects().all().filter<Package>(pck.get<Package>());
			for (auto& lib : libs) {
				auto source_files = SourceFile::objects().all().filter<Library>(lib);
				if (source_files.empty()) {
					continue;
				}

				std::vector<OriginSourceFile> origin_source_files;
				std::transform(source_files.begin(), source_files.end(), std::back_inserter(origin_source_files),
					[](const SourceFile& item) {return OriginSourceFile(item); });
				auto includes = IncludeRelationship::objects().all().filter<OriginSourceFile>(origin_source_files);
				
				auto targets_files = includes.value_list<TargetSourceFile>(); // TODO: Add select_related to queryset-cpp
				if (targets_files.empty()) {
					continue;
				}
				std::vector<std::size_t> targets_id;
				std::transform(targets_files.begin(), targets_files.end(), std::back_inserter(targets_id),
							  [](const TargetSourceFile& item){return item.value.get<0>();});
				auto targets = SourceFile::objects().all().filter<std::size_t>(targets_id);

				for (auto& target : targets) {
					auto lib_target = target.get<Library>();
					cpp_labels[lib_target.get_name()] = lib_target.get_name(false);
					cpp_labels[lib.get_name()] = lib.get_name(false);
					cpp_graph.push_back(std::make_pair(lib.get_name(), lib_target.get_name()));
				}
			}
		}

		// Generate the graph itself
		std::ostream* fp = &std::cout;
		std::ofstream fout;
		if (vm.find("output-filename") != vm.end()) {
			auto output_filename = vm["output-filename"].as<boost::filesystem::path>();
			if (boost::filesystem::exists(output_filename) && !override_output) {
				std::cerr << "File '" << output_filename << "' already exists. Use flag '--override' or delete it before.\n";
				return -1;
			}
			fout.open(output_filename.string());
			fp = &fout;
		}
		generate_graph(*fp, cmake_graph, cmake_labels, cpp_graph, cpp_labels);

    }
    catch (std::exception& e) {
        std::cerr << "Unhandled Exception reached the top of main: "
            << e.what() << ", application will now exit" << std::endl;
        return -1;
    }
    return 0;
}