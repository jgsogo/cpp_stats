
#pragma once

#include <vector>
#include <map>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

typedef std::vector<std::pair<std::string, std::string>> links_t;
typedef std::map<std::string, std::string> labels_t;

std::string find_node_id(labels_t& labels, const std::string& label, const std::string& id_if_empty) {
	auto it = std::find_if(labels.begin(), labels.end(), [&label](const std::pair<std::string, std::string>& item) { return item.second == label; });
	if (it != labels.end()) {
		return it->first;
	}
	labels.insert(std::make_pair(id_if_empty, label));
	return id_if_empty;
}

std::map<std::string, std::vector<std::string>> cluster_nodes(const labels_t& labels) {
	std::map<std::string, std::vector<std::string>> ret;
	for (const auto& item : labels) {
		std::vector<std::string> parts;
		boost::algorithm::split(parts, item.first, boost::is_any_of("/"));
		ret[*parts.begin()].push_back(item.first);
	}
	return ret;
}

void generate_graph(std::ostream& os, const links_t& cmake_edges, const labels_t& cmake_labels,
	const links_t& cpp_edges, const labels_t& cpp_labels) {
	auto graph_labels = cpp_labels;
	auto graph_edges = cpp_edges;

	// Map cmake_ids to cpp_ids.
	for (auto& cmake_edge : cmake_edges) {
		auto node_origin = find_node_id(graph_labels, cmake_labels.at(cmake_edge.first), cmake_edge.first);
		auto node_target = find_node_id(graph_labels, cmake_labels.at(cmake_edge.second), cmake_edge.second);
		graph_edges.push_back(std::make_pair(node_origin, node_target));
	}

	// Remove duplicates
	std::sort(graph_edges.begin(), graph_edges.end());
	graph_edges.erase(std::unique(graph_edges.begin(), graph_edges.end()), graph_edges.end());

	os << "digraph {" << std::endl;
	auto subgraphs = cluster_nodes(graph_labels);
	for (auto& subgraph : subgraphs) {
		os << "\tsubgraph cluster_" << subgraph.first << " {" << std::endl;
		os << "\t\tlabel = \"" << subgraph.first << "\";" << std::endl;
		for (auto& item : subgraph.second) {
			os << "\t\t\"" << item << "\" [ label=\"" << graph_labels.at(item) << "\"];" << std::endl;
		}
		os << "\t}" << std::endl;
	}
	for (auto& edge : graph_edges) {
		if (edge.first != edge.second) {
			os << "\t\"" << edge.first << "\" -> \"" << edge.second << "\"" << std::endl;
		}
	}
	os << "}" << std::endl;
}
