
#include <map>
#include <regex>

#include <boost/filesystem.hpp>

// Interesting, but format doesn't match: http://stackoverflow.com/questions/29496182/read-graphviz-in-boostgraph-pass-to-constructor

typedef std::vector<std::pair<std::string, std::string>> links_t;
typedef std::map<std::string, std::string> labels_t;

links_t read_graphviz(const boost::filesystem::path& filename, labels_t& labels) {
	std::smatch base_match;

	// Get labels
	{
		static std::regex re_edge("^\\s+\"(\\w+)\"\\s*\\[\\s*label=\"([\\w\\._-]+)\".*$");
		std::ifstream input(filename.string());
		for (std::string line; std::getline(input, line);)
		{
			if (std::regex_match(line, base_match, re_edge)) {
				//std::cout << base_match[1].str() << " -> " << base_match[2].str() << std::endl;
				labels.insert(std::make_pair(base_match[1].str(), base_match[2].str()));
			}
		}
	}

	// Get links
	links_t links;
	{
		static std::regex re_edge("^\\s+\"(\\w+)\"\\s*->\\s*\"(\\w+)\".*$");
		std::ifstream input(filename.string());
		for (std::string line; std::getline(input, line);)
		{
			if (std::regex_match(line, base_match, re_edge)) {
				//std::cout << base_match[1].str() << " -> " << base_match[2].str() << std::endl;
				links.push_back(std::make_pair(base_match[1].str(), base_match[2].str()));
			}
		}
	}
	return links;
}

