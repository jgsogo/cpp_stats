
#include <iostream>
#include <boost/program_options.hpp>

#include "../log_level_param.hpp"
#include "../../models/project.h"
#include "../../models/config_store.h"

#include "../../models/include_relationship.h"
#include "../../models/library.h"
#include "../../models/project.h"
#include "../../models/source_file.h"

using namespace cpp_stats;

int main(int argc, char** argv) {
    std::cout << "== Parse project ==\n";
    try {
        // Define and parse the program options
        namespace po = boost::program_options;
		bool override_db = false;
        po::options_description desc("Options");
		desc.add_options()
			("help", "Print help messages")
			("log-level,l", po::value<log_level>()->default_value(log_level(spdlog::level::info)), std::string("log level (" + log_level::options() + ")").c_str())
			("settings", po::value<std::string>()->required(), "path to settings file")
			//("outpath", po::value<std::string>()->required(), "output path (last directory will be created if not exists)")
			("project-path", po::value<boost::filesystem::path>()->required(), "path to project root")  // TODO: Make choices
			("override", po::bool_switch(&override_db), "override database if it already exists")
			;

        po::variables_map vm;
        try {
            po::store(po::parse_command_line(argc, argv, desc), vm); // can throw
                                                                     // handle help option
            if (vm.count("help")) {
                std::cout << "Basic Command Line Parameter App" << std::endl
                    << desc << std::endl;
                return 0;
            }
            po::notify(vm); // throws on error, so do after help in case there are any problems             
        }
        catch (po::error& e) {
            std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
            std::cerr << desc << std::endl;
            return -1;
        }

        // Get logger level (initialize also child loggers)
        spdlog::level::level_enum log_level = vm["log-level"].as<::log_level>()._level;
        //#ifdef SPDLOG_DEBUG_ON
        spdlog::stdout_logger_mt("qs")->set_level(log_level);
        //#endif
        auto console = spdlog::stdout_logger_mt("cpp_stats");
        console->set_level(log_level);

        // Get settings
        namespace fs = boost::filesystem;
        fs::path settings = vm["settings"].as<std::string>();
        std::cout << " - path to settings: '" << settings << "'\n";
        console->info("Configure C++ Stats: '{}'", settings.string());
        ConfigStore::get().parse_file(settings.string());

		// Create database if it doesn't exists
		auto sqlite3_db = ConfigStore::get().get_value_as<boost::filesystem::path>("database");
		if (!boost::filesystem::exists(sqlite3_db) || override_db) {
			console->info("Empty database at {}", sqlite3_db.string());
			boost::filesystem::remove(sqlite3_db);

			// TODO: May register models somewhere in "admin" app and have some commands there.
			auto& conn = ConfigStore::get().get_connection();
			SQLite3Manager<Project>::create_table(conn);
			SQLite3Manager<Package>::create_table(conn);
			SQLite3Manager<ProjectPackage>::create_table(conn);
			SQLite3Manager<Library>::create_table(conn);
			SQLite3Manager<SourceFile>::create_table(conn);
			SQLite3Manager<IncludeRelationship>::create_table(conn);
			
			console->debug("Database ready!");
		}

        // Parse project
        boost::filesystem::path project_path = vm["project-path"].as<boost::filesystem::path>();
        auto project = Project::objects().parse_project(project_path);

		console->info("Look for deps in project '{}'", project.get<std::string>());
		project.populate_deps();

    }
    catch (std::exception& e) {
        std::cerr << "Unhandled Exception reached the top of main: "
            << e.what() << ", application will now exit" << std::endl;
        return -1;
    }
    return 0;
}